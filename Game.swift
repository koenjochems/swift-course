//
//  Game.swift
//  MathTrainer
//
//  Created by test1 on 13/09/16.
//  Copyright © 2016 KEJA. All rights reserved.
//

import Foundation
import CoreData


class Game: NSManagedObject {

    convenience init(score:NSNumber, type:NSNumber, player:Player, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        let entity = NSEntityDescription.entityForName("Game", inManagedObjectContext: context)!
        self.init(entity: entity, insertIntoManagedObjectContext: context)
        
        self.score = score
        self.type = type
        self.player = player
    }

    var gameType: Games {
        get {
            return Games(rawValue: self.type!.integerValue)!
        }
        set {
            self.type = newValue.rawValue
        }
    }
}
