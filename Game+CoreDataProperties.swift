//
//  Game+CoreDataProperties.swift
//  MathTrainer
//
//  Created by test1 on 13/09/16.
//  Copyright © 2016 KEJA. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Game {

    @NSManaged var score: NSNumber?
    @NSManaged var type: NSNumber?
    @NSManaged var player: Player?

}
