//
//  ViewController.swift
//  MathTrainer
//
//  Created by test1 on 29/06/16.
//  Copyright © 2016 KEJA. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var userTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func deletePlayersPressed(sender: UIBarButtonItem) {
        if MathTrainerModel.mathTrainerModel.activePlayer != nil {
            MathTrainerModel.mathTrainerModel.delUser((MathTrainerModel.mathTrainerModel.activePlayer?.name)!)
            self.userTable.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MathTrainerModel.mathTrainerModel.players.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
        
        let user = MathTrainerModel.mathTrainerModel.players[indexPath.row]
        cell.textLabel?.text = "Name \(user.name) (\(user.getAge()))"
        cell.detailTextLabel?.text = "Level \(user.level)"
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        MathTrainerModel.mathTrainerModel.activePlayer = MathTrainerModel.mathTrainerModel.players[indexPath.row]
        performSegueWithIdentifier("selectPlayerAndShowGames", sender: self)
    }
}

