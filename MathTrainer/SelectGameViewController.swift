//
//  SelectGameViewController.swift
//  MathTrainer
//
//  Created by test1 on 29/06/16.
//  Copyright © 2016 KEJA. All rights reserved.
//

import UIKit

class SelectGameViewController: UIViewController {
    
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var playerLevelLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if (MathTrainerModel.mathTrainerModel.activePlayer != nil) {
            playerNameLabel.text = MathTrainerModel.mathTrainerModel.activePlayer?.name
            playerLevelLabel.text = MathTrainerModel.mathTrainerModel.activePlayer?.level.stringValue
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func AddGame(sender: UIButton) {
        if MathTrainerModel.mathTrainerModel.activePlayer != nil {
            MathTrainerModel.mathTrainerModel.selectGame(Games.ADD)
            performSegueWithIdentifier("showGameView", sender: self)
        }
    }

    @IBAction func GameSub(sender: UIButton) {
        if MathTrainerModel.mathTrainerModel.activePlayer != nil {
            MathTrainerModel.mathTrainerModel.selectGame(Games.SUB)
            performSegueWithIdentifier("showGameView", sender: self)
        }
    }
    
    @IBAction func GameDiv(sender: UIButton) {
        if MathTrainerModel.mathTrainerModel.activePlayer != nil {
            MathTrainerModel.mathTrainerModel.selectGame(Games.DIV)
            performSegueWithIdentifier("showGameView", sender: self)
        }
    }
    
    @IBAction func GameMul(sender: UIButton) {
        if MathTrainerModel.mathTrainerModel.activePlayer != nil {
            MathTrainerModel.mathTrainerModel.selectGame(Games.MUL)
            performSegueWithIdentifier("showGameView", sender: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
