//
//  MathTrainerModel.swift
//  MathTrainer
//
//  Created by test1 on 29/06/16.
//  Copyright © 2016 KEJA. All rights reserved.
//

import UIKit
import CoreData

enum Games: Int {
    case ADD = 1
    case SUB = 2
    case DIV = 3
    case MUL = 4
}

class MathTrainerModel: NSObject {
    static let mathTrainerModel = MathTrainerModel() // Singleton
    
    var coreDataStack: CoreDataStack = CoreDataStack(coreDataModelName: "MathTrainer")
    var players: [Player] = []
    var activePlayer: Player? = nil
    var activeGame: Game? = nil
    
    private override init() { //This prevents others from using the default '()' initializer for this class.
        let entityDescription = NSEntityDescription.entityForName("Player", inManagedObjectContext: self.coreDataStack.managedObjectContext)
        let request = FetchRequest<Player>(entity: entityDescription!)
        self.players = self.coreDataStack.loadContext(request)
    }
    
    func addUser(name: String, dateOfBirth: NSDate) {
        let player = Player(name: name, dateOfBirth: dateOfBirth, level: 1, games: NSSet(), insertIntoManagedObjectContext: self.coreDataStack.managedObjectContext)
        self.activePlayer = player
        self.players.append(player)
        
        self.coreDataStack.saveContext()
    }
    
    func delUser(name: String) {
        for player in self.players {
            if player.name == name {
                self.activePlayer = nil
                self.activeGame = nil
                self.coreDataStack.managedObjectContext.deleteObject(player)
                self.coreDataStack.saveContext()
                self.players.removeAtIndex(self.players.indexOf(player)!)
                break
            }
        }
    }
    
    func selectGame(game: Games) {
        for gameItem in self.activePlayer!.games! {
            if (gameItem as! Game).gameType == game {
                self.activeGame = gameItem as? Game
                return
            }
        }
        
        let gameNew = Game(score: 0, type: game.rawValue, player: self.activePlayer!, insertIntoManagedObjectContext: self.coreDataStack.managedObjectContext)
        self.activeGame = gameNew
    }
    
    func getGameName()->String {
        if self.activeGame == nil {
            return "unknown"
        }
        
        switch self.activeGame!.gameType {
        case Games.ADD:
            return "+"
        case Games.SUB:
            return "-"
        case Games.MUL:
            return "*"
        case Games.DIV:
            return "/"
        }
    }
    
    func getOperator()->Operator {
        switch self.activeGame!.gameType {
        case Games.ADD:
            return Operator.PLUS
        case Games.SUB:
            return Operator.MIN
        case Games.DIV:
            return Operator.DIV
        case Games.MUL:
            return Operator.MULTI
        }
    }
    
    func increaseLevel() {
        activePlayer?.level = NSNumber(int: activePlayer!.level.intValue + 1)
        activeGame?.score = 0

        self.coreDataStack.saveContext()
    }
}
