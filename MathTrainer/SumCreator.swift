//
//  SumCreator.swift
//  MathTrainer
//
//  Created by test1 on 29/06/16.
//  Copyright © 2016 KEJA. All rights reserved.
//

import UIKit

enum Operator {
    case PLUS, MIN, MULTI, DIV
}

struct Sum {
    var leftNum: UInt32 = 0
    var rightNum: UInt32 = 0
    var oper: Operator = Operator.PLUS
    var result: UInt32 = 0
}

class SumCreator: NSObject {
    var level: UInt32 = 1
    var oper: Operator
    
    init(level:UInt32, oper:Operator) {
        self.level = level
        self.oper = oper
    }
    
    func getMax()->UInt32 {
        switch self.oper {
        case Operator.PLUS:
            if self.level == 1 {
                return 10
            } else if self.level == 2 {
                return 20
            } else if self.level == 3 {
                return 50
            } else if self.level == 4 {
                return 100
            } else if self.level == 5 {
                return 1000
            } else {
                return 1000000
            }
        case Operator.MIN:
            if self.level == 1 {
                return 10
            } else if self.level == 2 {
                return 20
            } else if self.level == 3 {
                return 50
            } else if self.level == 4 {
                return 100
            } else if self.level == 5 {
                return 1000
            } else {
                return 1000000
            }
        case Operator.MULTI:
            if self.level == 1 {
                return 5
            } else if self.level == 2 {
                return 10
            } else if self.level == 3 {
                return 15
            } else if self.level == 4 {
                return 20
            } else if self.level == 5 {
                return 50
            } else {
                return 100
            }
        case Operator.DIV:
            if self.level == 1 {
                return 5
            } else if self.level == 2 {
                return 10
            } else if self.level == 3 {
                return 15
            } else if self.level == 4 {
                return 20
            } else if self.level == 5 {
                return 50
            } else {
                return 100
            }
        }
    }
    
    func create()->Sum {
        var sum = Sum()
        let max = self.getMax()
        
        switch self.oper {
        case Operator.PLUS:
            sum.leftNum = arc4random_uniform(max)
            sum.oper = self.oper
            sum.rightNum = arc4random_uniform(max - sum.leftNum)
            sum.result = sum.leftNum + sum.rightNum
        case Operator.MIN:
            sum.leftNum = arc4random_uniform(max)
            sum.oper = self.oper
            sum.rightNum = arc4random_uniform(sum.leftNum)
            sum.result = sum.leftNum - sum.rightNum
        case Operator.MULTI:
            sum.leftNum = arc4random_uniform(max)
            sum.oper = self.oper
            sum.rightNum = arc4random_uniform(10)
            sum.result = sum.leftNum * sum.rightNum
        case Operator.DIV:
            sum.rightNum = arc4random_uniform(10)
            sum.result = arc4random_uniform(max)
            sum.leftNum = sum.rightNum * sum.result
            sum.oper = self.oper
        }
        
        return sum
    }
    
}
