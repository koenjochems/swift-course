//
//  Person.swift
//  MathTrainer
//
//  Created by test1 on 29/06/16.
//  Copyright © 2016 KEJA. All rights reserved.
//

import Foundation
import CoreData


class Person: NSManagedObject {
    
//    init(name:String, dateOfBirth:NSDate, entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
//        super.init(entity: entity, insertIntoManagedObjectContext: context)
        
//        self.name = name
//        self.dateOfBirth = dateOfBirth

//    }

    convenience init(name:String, dateOfBirth:NSDate, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        let entity = NSEntityDescription.entityForName("Person", inManagedObjectContext: context)!
//        self.init(name: name, dateOfBirth: dateOfBirth, entity: entity, insertIntoManagedObjectContext: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)
        
        self.name = name
        self.dateOfBirth = dateOfBirth
    }
    
    func getAge()->Int {
        return Int(-self.dateOfBirth.timeIntervalSinceNow / 31536000)
    }
}
