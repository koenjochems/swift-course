//
//  Player.swift
//  MathTrainer
//
//  Created by test1 on 29/06/16.
//  Copyright © 2016 KEJA. All rights reserved.
//

import Foundation
import CoreData


class Player: Person {
    
    convenience init(name:String, dateOfBirth:NSDate, level:NSNumber, games:NSSet, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        let entity = NSEntityDescription.entityForName("Player", inManagedObjectContext: context)!
        self.init(entity: entity, insertIntoManagedObjectContext: context)
        
        self.name = name
        self.dateOfBirth = dateOfBirth
        self.level = level
        self.games = games
    }
}
