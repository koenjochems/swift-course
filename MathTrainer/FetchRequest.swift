//
//  FetchRequest.swift
//  MathTrainer
//
//  Created by test1 on 07/07/16.
//  Copyright © 2016 KEJA. All rights reserved.
//

import UIKit
import CoreData

class FetchRequest <T: NSManagedObject>: NSFetchRequest {
    init(entity: NSEntityDescription) {
        super.init()
        self.entity = entity
    }
}
