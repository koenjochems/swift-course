//
//  CreatePlayerViewController.swift
//  MathTrainer
//
//  Created by test1 on 29/06/16.
//  Copyright © 2016 KEJA. All rights reserved.
//

import UIKit

class CreatePlayerViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var dateOfBirthField: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func createPlayerPressed(sender: AnyObject) {
        MathTrainerModel.mathTrainerModel.addUser(nameField.text!, dateOfBirth: dateOfBirthField.date)
        
        performSegueWithIdentifier("createPlayerAndShowGames", sender: self)
    }
    
}
