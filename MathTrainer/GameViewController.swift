//
//  GameViewController.swift
//  MathTrainer
//
//  Created by test1 on 31/08/16.
//  Copyright © 2016 KEJA. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {

    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var playerLevelLabel: UILabel!
    @IBOutlet weak var gameTitle: UINavigationItem!
    
    @IBOutlet weak var gameSignLabel: UILabel!
    @IBOutlet weak var leftField: UITextField!
    @IBOutlet weak var rightField: UITextField!
    @IBOutlet weak var resultField: UITextField!
    
    var sumCreator: SumCreator? = nil
    var sum: Sum? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if (MathTrainerModel.mathTrainerModel.activePlayer != nil) {
            playerNameLabel.text = MathTrainerModel.mathTrainerModel.activePlayer?.name
            playerLevelLabel.text = MathTrainerModel.mathTrainerModel.activePlayer!.level.stringValue + " (" + MathTrainerModel.mathTrainerModel.activeGame!.score!.stringValue + ")"
            gameTitle.title = MathTrainerModel.mathTrainerModel.getGameName()
            gameSignLabel.text = MathTrainerModel.mathTrainerModel.getGameName()
            
            sumCreator = SumCreator(level: (MathTrainerModel.mathTrainerModel.activePlayer?.level.unsignedIntValue)!, oper: MathTrainerModel.mathTrainerModel.getOperator())
            sum = sumCreator?.create()
            leftField.text = "\(sum!.leftNum)"
            rightField.text = "\(sum!.rightNum)"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func press1(sender: UIButton) {
        resultField.text = resultField.text! + "1"
    }

    @IBAction func press2(sender: UIButton) {
        resultField.text = resultField.text! + "2"
    }
    
    @IBAction func press3(sender: UIButton) {
        resultField.text = resultField.text! + "3"
    }
    
    @IBAction func press4(sender: UIButton) {
        resultField.text = resultField.text! + "4"
    }
    
    @IBAction func press5(sender: UIButton) {
        resultField.text = resultField.text! + "5"
    }
    
    @IBAction func press6(sender: UIButton) {
        resultField.text = resultField.text! + "6"
    }
    
    @IBAction func press7(sender: UIButton) {
        resultField.text = resultField.text! + "7"
    }
    
    @IBAction func press8(sender: UIButton) {
        resultField.text = resultField.text! + "8"
    }
    
    @IBAction func press9(sender: UIButton) {
        resultField.text = resultField.text! + "9"
    }
    
    @IBAction func press0(sender: UIButton) {
        resultField.text = resultField.text! + "0"
    }
    
    @IBAction func pressBackspace(sender: UIButton) {
        if (resultField.text?.startIndex < resultField.text?.endIndex) {
            resultField.text?.removeAtIndex((resultField.text?.endIndex.predecessor())!)
        }
    }
    
    @IBAction func pressCheckResult(sender: UIButton) {
        if (resultField.text == "\(sum!.result)") {
            MathTrainerModel.mathTrainerModel.activeGame?.score = NSNumber(int: MathTrainerModel.mathTrainerModel.activeGame!.score!.intValue + 1)
            
            if MathTrainerModel.mathTrainerModel.activeGame?.score == 20 {
                MathTrainerModel.mathTrainerModel.increaseLevel()
                self.sumCreator = SumCreator(level: (MathTrainerModel.mathTrainerModel.activePlayer?.level.unsignedIntValue)!, oper: MathTrainerModel.mathTrainerModel.getOperator())
            }
            
            sum = sumCreator!.create()
            leftField.text = "\(sum!.leftNum)"
            rightField.text = "\(sum!.rightNum)"
            resultField.text = ""
            
            playerLevelLabel.text = MathTrainerModel.mathTrainerModel.activePlayer!.level.stringValue + " (" + MathTrainerModel.mathTrainerModel.activeGame!.score!.stringValue + ")"
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
