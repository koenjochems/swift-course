//
//  Player+CoreDataProperties.swift
//  MathTrainer
//
//  Created by test1 on 29/06/16.
//  Copyright © 2016 KEJA. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Player {

    @NSManaged var level: NSNumber
    @NSManaged var games: NSSet?

}
